FROM ruby:2.7-alpine

RUN apk update
RUN adduser -D docker
# Otherwise error extconf.rb failed
RUN apk add build-base

WORKDIR /home/docker/app

USER docker

COPY --chown=docker:docker Gemfile /home/docker/app/Gemfile
COPY --chown=docker:docker Gemfile.lock /home/docker/app/Gemfile.lock

RUN gem install bundler:2.2.14
# Allow gem groups to be included, production-only by default
ARG BUNDLE_WITH
ENV BUNDLE_WITH $BUNDLE_WITH
RUN bundle install

COPY --chown=docker:docker ./src /home/docker/app/src
COPY --chown=docker:docker ./config.ru /home/docker/app/config.ru
COPY --chown=docker:docker entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh

ENTRYPOINT ["entrypoint.sh"]
