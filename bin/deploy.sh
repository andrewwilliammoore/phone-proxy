#!/bin/bash

set -eu

echo "Dockerhub username:"
read docker_username

image_tag=$docker_username/phone_proxy:$(git rev-parse HEAD)

docker build -t $image_tag .
docker login -u $docker_username
docker push $image_tag

cd infrastructure
. ./.envrc
tfenv use

container_name=phone_proxy
export DOCKER_HOST="ssh://ubuntu@$(terraform output instance_public_ip | tr -d \")"

if docker container ls -a | grep $container_name; then
  docker stop $container_name
  docker rm $container_name
fi

port=4567
fqdn=$(terraform output app_fqdn | tr -d \")

echo "Deploying app image $image_tag"

docker run -p $port:$port \
           -e APP_ENV=production \
           -e DEPLOY_ENV=$DEPLOY_ENV \
           -e PORT=$port \
           -e VIRTUAL_HOST=$fqdn \
           -e LETSENCRYPT_HOST=$fqdn \
           -e VIRTUAL_PORT=$port \
           -e SENTRY_DSN=$SENTRY_DSN \
           -e TWILIO_ACCOUNT_SID=$TWILIO_ACCOUNT_SID \
           -e MAILGUN_API_KEY=$MAILGUN_API_KEY \
           -e EMAIL_FROM_ADDRESS=$EMAIL_FROM_ADDRESS \
           -e EMAIL_TO_ADDRESS=$EMAIL_TO_ADDRESS \
           --expose $port \
           --name $container_name \
           -d \
           $image_tag \
           bundle exec rackup --host 0.0.0.0 -p $port

echo "Finished deploying app"
