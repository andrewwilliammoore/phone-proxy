# frozen_string_literal: true

# Expects SENTRY_DSN environment variable
Sentry.init do |config|
  config.environment = ENV["DEPLOY_ENV"]
end
