# frozen_string_literal: true

require "sinatra"
require "sentry-ruby"

require "./src/config/index"
require "./src/mail_client"

class UnauthorizedError < StandardError; end

# Use error handling in all environments
disable :show_exceptions

helpers do
  def authorize_request!(params)
    raise UnauthorizedError if params["AccountSid"] != ENV["TWILIO_ACCOUNT_SID"]
  end
end

get "/" do
  "App is up!"
end

post "/message" do
  authorize_request! params

  MailClient.send params

  <<~TWIML
    <?xml version="1.0" encoding="UTF-8"?>
    <Response>
      <Message>Message Received</Message>
    </Response>
  TWIML
end

error UnauthorizedError do
  status 403
  "User is not authorized"
end

error 404 do
  "Route does not exist"
end

error do |error|
  status 500
  logger.error "#{error.class} #{error.message} #{error.backtrace}"
  Sentry.capture_exception(error)

  "There was an error"
end
