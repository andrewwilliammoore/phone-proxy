# frozen_string_literal: true

require "mailgun-ruby"

class MailClient
  class << self
    def send(payload)
      host = ENV["EMAIL_FROM_ADDRESS"].split("@")[1]
      params = {
        from: ENV["EMAIL_FROM_ADDRESS"],
        to: ENV["EMAIL_TO_ADDRESS"],
        subject: "SMS Received From #{payload['From']}",
        text: text_from_payload(payload)
      }

      instance.send_message host, params
    end

    private

    def instance
      @instance ||= Mailgun::Client.new ENV["MAILGUN_API_KEY"]
    end

    def text_from_payload(payload)
      payload.keys.reject { |k| k == "AccountSid" }.inject("") do |memo, key|
        memo + "#{key}: #{payload[key]}\n"
      end
    end
  end
end
