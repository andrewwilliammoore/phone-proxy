module "phone_proxy_compute_instance" {
  source = "https://andrewwilliammoore.gitlab.io/terraform-modules/openstack/compute-instance-with-docker/0.1.2.zip"

  providers = {
    openstack = openstack
  }

  app_name                = "phone-proxy"
  instance_public_ssh_key = var.INSTANCE_PUBLIC_SSH_KEY
}
