terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.40.0"
    }

    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.8.0"
    }
  }
}

provider "openstack" {}

provider "digitalocean" {
  token = var.DIGITAL_OCEAN_PERSONAL_ACCESS_TOKEN
}
