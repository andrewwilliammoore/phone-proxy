#!/bin/bash

set -eu

. ./.envrc
tfenv use

export DOCKER_HOST="ssh://ubuntu@$(terraform output instance_public_ip | tr -d \")"
fqdn=$(terraform output app_fqdn | tr -d \")

stop_existing_container () {
  container_name=$1

  if docker container ls -a | grep $container_name; then
    docker stop $container_name
    docker rm $container_name
  fi
}

nginx_proxy_name=nginx_tls_proxy
stop_existing_container $nginx_proxy_name
docker run -d \
           --name $nginx_proxy_name \
           -p 80:80 \
           -p 443:443 \
           -v certs:/etc/nginx/certs \
           -v vhost:/etc/nginx/vhost.d \
           -v html:/usr/share/nginx/html \
           -v /var/run/docker.sock:/tmp/docker.sock:ro \
           nginxproxy/nginx-proxy:0.9-alpine

nginx_proxy_acme_name=nginx-proxy-acme
stop_existing_container $nginx_proxy_acme_name
docker run -d \
           --name $nginx_proxy_acme_name \
           --volumes-from $nginx_proxy_name \
           -v /var/run/docker.sock:/var/run/docker.sock:ro \
           -v acme:/etc/acme.sh \
           -e "DEFAULT_EMAIL=$LETSENCRYPT_CONTACT_EMAIL" \
           nginxproxy/acme-companion:2.1
