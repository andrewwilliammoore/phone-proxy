data "digitalocean_domain" "app_domain" {
  name = var.APP_DOMAIN
}

resource "digitalocean_record" "app_subdomain" {
  domain = data.digitalocean_domain.app_domain.name
  type   = "A"
  name   = var.APP_SUBDOMAIN
  value = module.phone_proxy_compute_instance.floating_ip
}
