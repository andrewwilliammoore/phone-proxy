output "instance_public_ip" {
  value = module.phone_proxy_compute_instance.floating_ip
}

output "app_fqdn" {
  value = digitalocean_record.app_subdomain.fqdn
}
