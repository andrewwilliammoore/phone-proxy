## Setup
Uses Openstack and Digital Ocean.

### Environment Variables
Place these in a .envrc file:

- Get credentials from your OpenStack provider. For example, download an OpenRc.sh file.
- Generate an ssh key. Copy the value of the public key.
- Assumes that you already have a domain registered and have name servers on DigitalOcean.
```bash
# Openstack

# To use an OpenStack cloud you need to authenticate against the Identity
# service named keystone, which returns a **Token** and **Service Catalog**.
# The catalog contains the endpoints for all services the user/tenant has
# access to - such as Compute, Image Service, Identity, Object Storage, Block
# Storage, and Networking (code-named nova, glance, keystone, swift,
# cinder, and neutron).
export OS_AUTH_URL=

# In addition to the owning entity (tenant), OpenStack stores the entity
# performing the action as the **user**.
export OS_APPLICATION_CREDENTIAL_ID=

# With Keystone you pass the keystone application_credential_secret.
export OS_APPLICATION_CREDENTIAL_SECRET=

# If your configuration has multiple regions, we set that information here.
# OS_REGION_NAME is optional and only valid in certain environments.
export OS_REGION_NAME=""
# Don't leave a blank variable, unset it if it was empty
if [ -z "$OS_REGION_NAME" ]; then unset OS_REGION_NAME; fi

export OS_AUTH_TYPE="v3applicationcredential"
export OS_IDENTITY_API_VERSION=3

# Public key of ssh key you created
export TF_VAR_INSTANCE_PUBLIC_SSH_KEY=""

# Digital Ocean
export TF_VAR_DIGITAL_OCEAN_PERSONAL_ACCESS_TOKEN=...;

export TF_VAR_APP_DOMAIN=...; # Root domain for the app. This is assumed to already exist
export TF_VAR_APP_SUBDOMAIN=...; # subdomain for accessing the service

export LETSENCRYPT_CONTACT_EMAIL=...; # Email address where you can be reached by letsencrypt

# Monitoring
export SENTRY_DSN=...; # Uses sentry.io for error monitoring

# Twilio
export TWILIO_ACCOUNT_SID=...;

# Mailgun
export MAILGUN_API_KEY=...;

# For forwarding incoming messages to email
export EMAIL_FROM_ADDRESS=...;
export EMAIL_TO_ADDRESS=...;

# For Deployment Config
export DEPLOY_ENV=...; # i.e. staging/production, etc.
```

Load environment variables:
```bash
. .envrc
```

## Apply Infrastructure
```bash
tfenv use
terraform init
terraform apply
```
It will take a while for the instance to boot up and be provisioned. Terraform has no way of knowing when this is complete.

### Read Provisioning Logs
ssh into instance (see below). Read cloud-init logs. cloud-init is used to provision the instance:
```bash
tail -f /var/log/cloud-init-output.log
```

## Establish ssh connection
ssh into the instance:
```bash
ssh -i PATH_TO_YOUR_SSH_PRIVATE_KEY ubuntu@$(terraform output instance_public_ip | tr -d \")
```

## Operate Docker remotely:
```bash
DOCKER_HOST="ssh://ubuntu@$(terraform output instance_public_ip | tr -d \")" docker image ls
```

## Add TLS encryption
```bash
./bin/encrypt.sh
```
