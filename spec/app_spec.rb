# frozen_string_literal: true

require "./src/index" # <-- your sinatra app
require "rspec"
require "rack/test"
require "pry"

RSpec.describe "Phone-Proxy App" do
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  describe "GET non-existent route" do
    it "responds with 404" do
      get "/FAKE-PATH"

      expect(last_response.status).to eq(404)
      expect(last_response.body).to eq("Route does not exist")
    end
  end

  describe "GET /" do
    it "responds successfully" do
      get "/"

      expect(last_response.status).to eq(200)
      expect(last_response.body).to eq("App is up!")
    end
  end

  describe "POST /message" do
    let(:mock_client) { instance_double(Mailgun::Client) }

    before do
      allow(mock_client).to receive(:send_message)
      allow(Mailgun::Client).to receive(:new) { mock_client }
      allow(Sentry).to receive(:capture_exception)
    end

    # Due to singleton being memoized, but the mock being set per-test
    # https://stackoverflow.com/a/50293581
    after { MailClient.instance_variable_set("@instance", nil) }

    it "responds with unauthorized error" do
      post "/message"

      expect(last_response.status).to eq(403)
      expect(last_response.body).to eq("User is not authorized")
    end

    describe "with authorization" do
      let(:params) do
        {
          "AccountSid" => ENV["TWILIO_ACCOUNT_SID"],
          "From" => "+16151234567", # Not yet sure on the format that the phone number will be shown
          "To" => "+490301234567",
          "Body" => "Some text message"
        }
      end

      context "when successfull" do
        before do
          post "/message", params
        end

        it "responds successfully" do
          expect(last_response.status).to eq(200)
          expect(last_response.body).to eq(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"\
            "<Response>\n"\
            "  <Message>Message Received</Message>\n"\
            "</Response>\n"
          )
        end

        it "sends email" do
          host = "test.local"
          expect(mock_client).to have_received(:send_message).with(
            host,
            {
              to: "recipient@#{host}",
              from: "phone.proxy.service@test.local",
              subject: "SMS Received From #{params['From']}",
              text: "From: #{params['From']}\nTo: #{params['To']}\nBody: #{params['Body']}\n"
            }
          )
        end
      end

      context "when a server error occurs" do
        let(:error) { ArgumentError.new "Oops" }

        before do
          allow(mock_client).to receive(:send_message).and_raise(error)

          post "/message", params
        end

        it "responds with error" do
          expect(last_response.status).to eq(500)
          expect(last_response.body).to eq("There was an error")
        end

        it "alerts error monitoring service" do
          expect(Sentry).to have_received(:capture_exception).with(error)
        end
      end
    end
  end
end
