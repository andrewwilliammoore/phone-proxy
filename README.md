# Phone Proxy
Receive SMS messages

## Infrastructure
See [infrastructure](./infrastructure)

## Run app locally
```bash
docker-compose up
```
App will be available at http://localhost:4567 (if using default port)

### Run tests
```bash
docker-compose exec web bundle exec rspec
```

### Run linter
```bash
docker-compose exec web bundle exec rubocop
```

## Deploy App
Requires ssh access to the instance created in infrastructure.
```bash
./bin/deploy.sh
```
